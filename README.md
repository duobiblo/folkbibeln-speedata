This is a test for bible publishing with speedata Publisher.

It combines the text of Svenska Folkbibeln 2015 with images tagged with "Sweden" from Unsplash.

## What works:
- defining font attributes
- integrate images into the document
- layout in 2 columns
- print page number in header (max page needs a 2nd pass)
- show current book and chapter in header
- have different page layouts for even and odd pages
- kerning
- hyphenation
- verse numbers should move to the next line when they are the last element in a line (horizontal orphan)
- columns have different heights (two columns do not end at the same vertical line)


## What does not work yet:
- print the first verse of an even current page in header
- print the last verse of an odd current page in header
- print the chapter number (including the image for every tenth chapter) on the next page or column if its the last line (orphan)
- Invert color of black text over a dark background
 